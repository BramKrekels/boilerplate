import axios from 'axios';

const spaceId = process.env.CONTENTFUL_SPACE_ID;
const accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;
const contentfulUrl = `https://cdn.contentful.com/spaces/${spaceId}/environments/master/entries?access_token=${accessToken}&content_type=page&fields.slug=`;

export const getData = (slug) => {
    return axios.get(`${contentfulUrl}${slug}`)
        .then(res => {
            return (res.data.includes.Entry[0]);
        })
        .catch(err => {
            console.log(err);
            return null;
        });
}
