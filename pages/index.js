import React from 'react'
import Head from 'next/head';
import { getData } from '../helpers/RequestData'
import { HeaderComponent } from '../components/Header';
import { FooterComponent } from '../components/Footer';
import { ComponentLoader } from '../components/ComponentLoader';
import { ErrorComponent } from '../components/Error';
import "./style.scss"

export class Page extends React.Component {
    static async getInitialProps({ req }) {
        let slug = req.url.replace(/\//g, '');

        let content = await getData(slug).then(data => {
            if (data) {
                console.log(data);
                return data;
            } else {
                return 'error'
            }
        })

        if (content !== 'error') {
            content.slug = slug;
        }

        return {
            content
        };
    }

    //TODO: Pass component types to ComponentLoader
    render() {
        const pageProps = this.props.content.fields;
        const pageTitle = this.props.content.slug;

        return (
            <div className="page">
                <Head>
                    <title>Boilerplate - {pageTitle} </title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>

                <HeaderComponent />
                {pageProps ? (
                    <ComponentLoader {...pageProps} />
                ) : (
                        <ErrorComponent />
                    )
                }
                <FooterComponent />
            </div>
        )
    }
}

export default Page
