const { createServer } = require('http')
const next = require('next')
const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const { parse } = require('url')

app.prepare().then(() => {
    createServer((req, res) => {
        const parsedUrl = parse(req.url, true)
        const { pathname } = parsedUrl

        //TODO: Display favicon and make robots.txt available
        if (pathname === '/favicon.ico') {
            console.log('requesting favicon');
        } else if (pathname === '/robots.txt') {
            console.log('requesting robots');
        } else {
            app.render(req, res, '/index')
        }
    }).listen(port, err => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
})